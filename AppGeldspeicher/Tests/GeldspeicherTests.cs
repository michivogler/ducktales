using System;
using Ducktales.Domain;
using Xunit;

namespace Ducktales.Tests
{
    public class GeldspeicherTests
    {
        [Fact]
        public void SummeTest()
        {
            var geldspeicher = new Geldspeicher(int.MaxValue);

            Assert.Equal(int.MaxValue, geldspeicher.Summe);
        }
    }
}
