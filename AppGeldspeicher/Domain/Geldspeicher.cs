﻿using System;

namespace Ducktales.Domain
{
    public class Geldspeicher
    {
        public Geldspeicher(int summe)
        {
            Summe = summe;
        }

        public int Summe { get; }
    }
}
